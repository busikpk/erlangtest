%%%-------------------------------------------------------------------
%%% @author Bohdan
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 18. квіт 2015 22:17
%%%-------------------------------------------------------------------
-module(basics).
-author("Bohdan").

%% API
-export([factorial/1,fast_fact/1,case_test/1,is_polindrom/1,max/1]).

factorial(0)->1;
factorial(N)->N*factorial(N-1).

fast_fact(N)->fac(N,1).

fac(0,Res)->Res;
fac(N,Res)->fac(N-1,N*Res).


case_test(X)->
  case X of
    {1,2}->true;
    {1,3}->false;
    {_,_} -> wow;
    1 -> one
  end.

is_polindrom(List) when ((tl(List)==[]) or (List == [])) -> true;
is_polindrom([H|T])->
  Last = lists:last(T),
  if H == Last -> is_polindrom(lists:sublist(T,length(T)-1));
    true -> false
  end.

max(List)->
  if
    List == [] -> empty_LIST_can*t_find_max;
    true -> my_max(List,hd(List))
  end.

my_max([],Max)->Max;
my_max([H|T],Max) ->
  if
    H > Max -> my_max(T,H);
    true -> my_max(T,Max)
  end.



