%%%-------------------------------------------------------------------
%%% @author Bohdan
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 16. квіт 2015 11:30
%%%-------------------------------------------------------------------
-module(test).
-export([hello_world/0]).

hello_world() -> io:fwrite("hello, world\n").
