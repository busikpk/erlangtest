%%%-------------------------------------------------------------------
%%% @author Bohdan
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 16. квіт 2015 11:52
%%%-------------------------------------------------------------------
-module(main).
-author("Bohdan").

%% API
%% coding:UTF-8
%%[print_helloworld/0, kolyaloh/0,test/1,mnogArr/1]
-export([]).

print_helloworld()->
  kolyaloh(),
  io:write(1).

kolyaloh()->
  io:format("kola loh~n").

test(Ar)->
  Ans = sum(Ar)+1,
  io:write(Ans).

sum([])->[];
sum([H|T])-> H + sum(T).

mnogArr([])->0;
mnogArr([H|T])->[H*3|mnogArr(T)].

