%%%-------------------------------------------------------------------
%%% @author Bohdan
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 20. квіт 2015 20:55
%%%-------------------------------------------------------------------
-module(sprob).
-author("Bohdan").

%% API
-export([last/1,lastB/1,get/2,len/1,revers/1,duplicate/1,drop/2,duplicate2/2,split/2,
        splitTwo/3,removeK/2,rotate/2,duplicate3/1,flatten/1,duplicateInList/1,lenOfDup/1,
        lenOfDupWithIf/1,toList2/1,insertAt/3,range/2,rand/2,randomSelect/2,randomPermute/1,
        bubbleSort/1]).

% Find the last element of a list.
last([H|T])-> last(T,H).
last([],Last)->Last;
last([H|T],Last)->last(T,H).

% Find the last but one element of a list.
lastB(L)->
  if
    length(L) == 1 -> hd(L) ;
    true ->  last1(L)
  end.
last1([H|T])->
  if
    length(T) == 1 -> H;
    true -> last1(T)
  end.

% Find the Kth element of a list.
get(List, K)->getEl(List,K,0).
getEl([H|T],K,I)->
  if
    K == I -> H ;
    true -> getEl(T,K,I+1)
  end.

% Find the number of elements of a list.
len(List)->count(List,0).
count([],Count)->Count;
count([H|T],Count)->
  count(T,Count+1).

% Reverse a list.
revers(List)->rev(List,[]).
rev([],ReversList)->ReversList;
rev([H|T],ReversList)->rev(T,[H|ReversList]).

% Find out whether a list is a palindrome.
is_polindrom(List) when ((tl(List)==[]) or (List == [])) -> true;
is_polindrom([H|T])->
  Last = lists:last(T),
  if H == Last -> is_polindrom(lists:sublist(T,length(T)-1));
    true -> false
  end.

% Duplicate the elements of a list.
duplicate(List)->duplic(List,[]).
duplic([],Res)->Res;
duplic([H|T],Res)->duplic(T,Res++[H,H]).

%  Drop every Nth element from a list.
drop(List,Nth)->droper(List,[],Nth,0).
droper([],Res,N,Time)->Res;
droper([H|T],Res,N,Time)->
  if
    Time == N -> droper(T,Res,N,0) ;
    true -> droper(T,Res++[H],N,Time+1)
  end.


% Duplicate the elements of a list a given number of times.
duplicate2(Times,List)->duplic2(List,[],Times).
repyter(H,Times,Count,Res)->
  if
    Times == Count -> Res ;
    true -> repyter(H,Times,Count+1,Res++[H])
  end.
duplic2([],Res,Times)->Res;
duplic2([H|T],Res,Times)->duplic2(T,Res++repyter(H,Times,0,[]),Times).

split(Fl,List)->spliter(Fl,List,[],0).
spliter(Fl,[H|T],Res,Count)->
  if
    Fl == Count ->[Res|[T]];
    true -> spliter(Fl,T,Res++[H],Count+1)
  end.

% Extract a slice from a list.
splitTwo(Start,End,List)->spl(Start,End,List,0,[]).

spl(Start,End,[],Index,Res)->Res;
spl(Start,End,[H|T],Index,Res)->
  if
    Start =< Index andalso Index < End -> spl(Start,End,T,Index+1,Res++[H]);
    End == Index-> Res;
    true -> spl(Start,End,T,Index+1,[])
  end.

%Remove the Kth element from a list.
removeK(List,K)->remover(List,[],K,0).
remover([],Res,K,Index)-> Res;
remover([H|T],Res,K,Index)->
  if
    K == Index-> remover(T,[[H]]++Res,K,Index+1);
    true ->  remover(T,Res++[H],K,Index+1)
  end.

%Rotate a list N places to the left.
rotate(List,N)->
  if
    N >= 0 -> lists:sublist(List, N+1, length(List) - N) ++ lists:sublist(List, N);
    true -> lists:sublist(List, length(List) - abs(N) + 1, abs(N)) ++
            lists:sublist(List, 1, length(List) - abs(N))
  end.

% Eliminate consecutive duplicates of list elements.
duplicate3(List)-> dup(List,[hd(List)]).
dup([],Res) -> Res;
dup([First],Res) -> Res;
dup([H|T],Res)->
  L = lists:last(Res),
  if
    H == L -> dup(T,Res);
    true ->  dup(T,Res ++ [H])
  end.

%Flatten a nested list structure.
flatten([])->[] ;
flatten([[]|T])-> flatten(T);
flatten([[H|T]|T2])-> flatten([H|[T|T2]]);
flatten([H|T])-> [H|flatten(T)].

duplicateInList(L)->duplicateInList(L,[],[],hd(L)).
duplicateInList([],BufL,Res,Val)->Res++[[Val]];
duplicateInList([H|T],BufL,Res,Val)->
  if
    H==Val -> duplicateInList(T,BufL++[H],Res,H);
    true -> duplicateInList(T,[H],Res++[BufL],H)
  end.

%Run-length encoding of a list.
lenOfDup(L)->
  Target = duplicateInList(L),
  lenOfDup(Target,[]).
lenOfDup([],Res)->Res;
lenOfDup([H|T],Res)-> lenOfDup(T,Res++[[{hd(H)},length(H)]]).


%Modified run-length encoding
lenOfDupWithIf(L)->
  Target = lenOfDup(L),
  lenOfDupWithIf(Target,[]).
lenOfDupWithIf([],Res)->Res;
lenOfDupWithIf([H|T],Res)->
  Tmp = lists:last(H),
  F = hd(H),
  {Ans} = F,
  if
    Tmp == 1 -> lenOfDupWithIf(T,Res++[Ans]);
    true -> lenOfDupWithIf(T,Res++[[hd(H),length(H)]])
  end.

% Decode a run-length encoded list
toList2(L)->toList2(L,[]).
valueGivenTimes(Times,Value,ResBuf)->
  if
    Times == 0 ->ResBuf ;
    true -> valueGivenTimes(Times-1,Value,ResBuf++Value)
  end.
toList2([],Res) -> Res;
toList2([H|T],Res)->
  Res ++ valueGivenTimes(hd(H),tl(H),[])++toList2(T,Res).


% Insert an element at a given position into a list
insertAt(Element,Pos,List)->insertAt(Element,Pos,1,List,[]).
insertAt(Element,Pos,Index,[],Res)->Res;
insertAt(Element,Pos,Index,[H|T],Res)->
  if
    Pos == Index -> Res++[H,Element]++insertAt(Element,Pos,Index+1,T,Res);
    true -> Res++[H]++insertAt(Element,Pos,Index+1,T,Res)
  end.

% Create a list containing all integers within a given range.
range(Start,End)-> range(Start,End,[Start]).
range(Start,End,Res)->
  if
    Start < End-> Res++[Start+1]++range(Start+1,End,[]);
    true -> Res
  end.

%Lotto: Draw N different random numbers from the set 1..M
rand(N,Max)-> rand(N,Max,[]).
rand(N,Max,Res)->
  if
    length(Res) < N -> rand(N,Max,Res++[random:uniform(Max)]);
    true -> Res
  end.

%  Extract a given number of randomly selected elements from a list.
randomSelect(N,List)->randomSelect(N,List,[]).
randomSelect(N,List,Res)->
  Len = length(Res),
  LenList = length(List)-1,
  Index = random:uniform(LenList),
  Value = get(List,Index),
  if
    Len < N -> randomSelect(N,List,Res++[Value]) ;
    true -> Res
  end.

% Generate a random permutation of the of a lis
randomPermute(List)->randomSelect(length(List),List).


bubbleSort(List)->bubbleSortRun(List,length(List),0).
bubbleSortRun(List,Len,Index)->
  Tmp = sortB(List),
  if
    Len == Index -> Tmp;
    true -> bubbleSortRun(Tmp,Len,Index+1)
  end.
sortB([]) -> [];
sortB([H]) -> [H];
sortB([H1,H2|T]) ->
  if
    H1 > H2 -> [H2] ++ sortB([H1] ++ T);
    true -> [H1] ++ sortB([H2] ++ T)
  end.